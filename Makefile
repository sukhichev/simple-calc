# Определение среды сборки
ifeq ($(OS),Windows_NT)
  ifeq ($(shell uname -s),) # not in a bash-like shell
	CLEANUP = del /F /Q
	MKDIR = mkdir
  else # in a bash-like shell, like msys
	CLEANUP = rm -f
	MKDIR = mkdir -p
  endif
	TARGET_EXTENSION=.exe
else
	CLEANUP = rm -f
	MKDIR = mkdir -p
	TARGET_EXTENSION=
endif

# Имя программы
MAIN := simple-calc


.PHONY: clean clean-all
.PHONY: test all build

#################### Структура каталогов ######################
PATHU = unity/src/
PATHS = src/
PATHT = test/
PATHB = build/
PATHD = build/depends/
PATHO = build/objs/
PATHR = build/results/

BUILD_PATHS = $(PATHB) $(PATHD) $(PATHO) $(PATHR)

# Файлы основной сборки
SRC := $(wildcard $(PATHS)*.c)
OBJ := $(patsubst $(PATHS)%.c,$(PATHO)%.o,$(SRC))
HEADERS := $(wildcard $(PATHS)*.h)

# Файлы тестов
## Файлы тестов должны называться как тестируемый файл с приставкой "Test_"
SRCT = $(wildcard $(PATHT)*.c)
TESTRESULTS = $(patsubst $(PATHT)Test_%.c,$(PATHR)Test_%.txt,$(SRCT) )


######### Используемые компиляторы (должен определить сам) #############
#COMPILE.c=gcc -c
#LINK.c=gcc
#DEPEND=gcc -MM -MG -MF

# Общие флаги компилятора
CFLAGS += -I$(PATHS) # Расположение файлов заголовков
CFLAGS += --std=c99   # Используемый стандарт
TESTCFLAGS:= $(CFLAGS) 
UNITYCFLAGS:= $(CFLAGS)
# Специфичные для основной сборки
CFLAGS += -Wall -Wmissing-prototypes -Wmissing-declarations -pedantic # Ошибки
# Специфичные для тестов
TESTCFLAGS += -I. -I$(PATHU) # Расположение файлов заголовков
TESTCFLAGS += -DTEST # Макросы
# Специфичные сборки Unity
UNITYCFLAGS += -DUNITY_INCLUDE_DOUBLE # Макросы

SOFLAGS= -shared -fpic



all: build test

########################## Основная сборка ######################

build: $(BUILD_PATHS) $(PATHB)$(MAIN)$(TARGET_EXTENSION)
	@echo "============== Build complete =============="

$(PATHB)$(MAIN)$(TARGET_EXTENSION): $(OBJ)

$(PATHO)$(MAIN).o: $(SRC) $(HEADERS)

######################### Тестирование ######################
PASSED = `grep -s PASS $(PATHR)*.txt`
FAIL = `grep -s FAIL $(PATHR)*.txt`
IGNORE = `grep -s IGNORE $(PATHR)*.txt`

test: $(BUILD_PATHS) $(TESTRESULTS)
	@echo "-----------------------\nIGNORES:\n-----------------------"
	@echo "$(IGNORE)"
	@echo "-----------------------\nPASSED:\n-----------------------"
	@echo "$(PASSED)"
	@echo "-----------------------\nFAILURES:\n-----------------------"
	@echo "$(FAIL)"
	@echo "\nDONE"

######################### Неявные правила ######################

$(PATHR)%.txt: $(PATHB)%$(TARGET_EXTENSION)
	-./$< > $@ 2>&1

$(PATHB)Test_%$(TARGET_EXTENSION): $(PATHO)Test_%.o $(PATHO)%.o $(PATHU)unity.o #$(PATHD)Test%.d
	$(LINK.c) -o $@ $^

$(PATHB)%$(TARGET_EXTENSION): $(PATHO)%.o
	$(LINK.c) -o $@ $^


$(PATHO)%.o:: $(PATHT)%.c
	$(COMPILE.c) $(TESTCFLAGS) $< -o $@

$(PATHO)%.o:: $(PATHS)%.c $(HEADERS)
	$(COMPILE.c) $(CFLAGS) $< -o $@

$(PATHO)%.o:: $(PATHU)%.c $(PATHU)%.h
	$(COMPILE.c) $(UNITYCFLAGS) $< -o $@

$(PATHU)%.o: $(PATHU)%.c $(PATHU)%.h
	$(COMPILE.c) $(UNITYCFLAGS) $< -o $@

$(PATHD)%.d:: $(PATHT)%.c
	$(DEPEND) $@ $<

$(PATHB):
	$(MKDIR) $(PATHB)

$(PATHD):
	$(MKDIR) $(PATHD)

$(PATHO):
	$(MKDIR) $(PATHO)

$(PATHR):
	$(MKDIR) $(PATHR)


########################## Чистка ######################

clean:
	$(CLEANUP) $(PATHO)*.o
	$(CLEANUP) $(PATHB)Test_*$(TARGET_EXTENSION)
	$(CLEANUP) $(PATHB)$(MAIN)$(TARGET_EXTENSION)
	$(CLEANUP) $(PATHR)*.txt

clean-all: clean
	$(CLEANUP) $(PATHB)

.PRECIOUS: $(PATHB)Test_%$(TARGET_EXTENSION)
.PRECIOUS: $(PATHB)$(MAIN)$(TARGET_EXTENSION)
.PRECIOUS: $(PATHD)%.d
.PRECIOUS: $(PATHO)%.o
.PRECIOUS: $(PATHR)%.txt
