/*
 * This is Russian description. English description see below.
 * 
 * Печать данных лексического и синтаксический анализаторов
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * О лицензии читай в файле simple-calc.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Print data of tokinizer and parser
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of simple calc package.
 * 
 * See Copyright in simple-calc.h
 *  
 */


#include <stdio.h>
/* Для printf() */
#include <stdio.h>

#include "parserview.h"

/* Строки названий типов и имён токенов */
#undef DEF_TOKEN
#define DEF_TOKEN(a) #a,
char token_type_str[][80] = {
#include "token_type.def"
};
char token_name_str[][80] = {
#include "token_name.def"
};


void _draw_tree(AST_node_t* node_arr, size_t index, int level, char *str, char ch);

void print_token (token_t tok)
{
	printf("%s", token_type_str[tok.type]);
	
	switch (tok.type){
		case STOP:
		case ERROR:
		case LBR:
		case RBR:
		case SPACE:
			break;
		case NUMBER:
			printf(": %lf", tok.u.val.dval);
			break;
		case DELIMITER:
			printf(": %s", token_name_str[tok.u.name]);
			break;
		case STRING:
		case ID:
			printf("Not implement yet");
			break;
		default:
			printf("Something wrong!");
	}
	
	printf("\n");
	
	return;
}

void print_token_array (token_t* tok_arr)
{
	if (!tok_arr){
		printf("ERROR: Except token array pointer but got NULL.");
		return;
	}
	while (tok_arr->type != STOP)
	{
		print_token(*tok_arr);
		tok_arr++;
	}
	return;
}



void _draw_tree(AST_node_t* node_arr, size_t index, int level, char *str, char ch)
{
	if(index){
		AST_node_t current = node_arr[index];
		printf("%s", str);
		printf(">");
		print_token(*current.node);
		if(level > 0) str[2*level - 1] = ch;
		str[2*level]=' ';
		str[2*level + 1]='+';
		str[2*level + 2]='\0';
		_draw_tree(node_arr, current.left, level + 1, str, '|');
		if(level > 0) str[2*level - 1] = ch;
		str[2*level]=' ';
		str[2*level + 1]='L';
		str[2*level + 2]='\0';
		_draw_tree(node_arr, current.right, level + 1, str, ' ');
		
	}
	return;
}

void draw_tree(AST_t tree)
{
	char str[80];
	str[0]='\0';
	_draw_tree(tree.node_arr, tree.root, 0, str, ' ');
	return;
}

void print_tree(AST_t tree)
{
	int i;
	AST_node_t elem;
	printf("Capacity: %zd; Length: %zd\n", tree.data_capacity, tree.len);
	printf("Root index: %zd\n", tree.root);
	for (i = 1; i<=tree.len; i++)
	{
		elem = tree.node_arr[i];
		printf("%d) Left: %zd; Right: %zd; ", i, elem.left, elem.right);
		print_token(*elem.node);
	}
	return;
}

