/*
 * This is Russian description. English description see below.
 * 
 * Заголовочный файл печати данных лексического и синтаксический анализаторов
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * О лицензии читай в файле simple-calc.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Headers of print data of tokinizer and parser
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of simple calc package.
 * 
 * See Copyright in simple-calc.h
 *  
 */

#ifndef _PARSERVIEW_H
#define _PARSERVIEW_H	1

#include "tokinizer.h"
#include "parser.h"


void print_token (token_t tok);
void print_token_array (token_t* tok_arr);
void print_tree(AST_t tree);
void draw_tree(AST_t tree);


#endif /* parserview.h */
