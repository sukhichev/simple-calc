/*
 * This is Russian description. English description see below.
 * 
 * Синтаксический анализатор
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * О лицензии читай в файле simple-calc.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Parser
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of simple calc package.
 * 
 * See Copyright in simple-calc.h
 *  
 */

#include <stdlib.h>
/* Для printf() */
#include <stdio.h>

#include "parser.h"

/** @brief Синтаксический анализатор
 * 
 * Это файл состоит из двух частей: реализации дерева и реализации собственно
 * синтаксического анализатора.
 * 
 * Дерево реализовано довольно традиционно. Единственное отличие в том, что 
 * вместо указателей на левую и правую ветвь используются индексы. Вызвано это 
 * тем, что дерево реализовано на основе динамически расширяемого массива. 
 * Такая реализация призвана упростить реализацию дерева, а так же сократить 
 * количество дорогих операций выделения памяти. Однако при запросе на 
 * расширение выделяемой памяти функцией realloc() может поменяться адрес
 * области выделяемой памяти. После такой операции указатели будут ссылаться 
 * на старые участки памяти, а потому будут неверны. Индексы же независимы от
 * положения массива в памяти. Недостатком такой реализации является меньшая 
 * эффективность обхода дерева, так как указатели вычисляются динамически, а не
 * хранятся в массиве.
 * 
 * Из-за этого вместо указателя NULL используется индекс 0. Таким образом, 
 * чтобы избежать путаницы, в 0 элемент массива node_arr ничего не записывается.
 * 
 * */

/* Внутренний интерфейс */
size_t parse_E (AST_t* tree, token_t** tok_p);
size_t parse_T1 (AST_t* tree, token_t** tok_p);
size_t parse_T2 (AST_t* tree, token_t** tok_p);
size_t parse_T3 (AST_t* tree, token_t** tok_p);


int AST_init(AST_t* tree)
{
	tree->node_arr = (AST_node_t*) malloc(INIT_AST_CAPACITY * sizeof(AST_node_t));
	if (tree->node_arr == NULL)
		{ return (1); }
	tree->data_capacity = INIT_AST_CAPACITY;
	
	tree->len = 0;
	tree->root = 0;
	return (0);
}

int AST_free(AST_t *tree)
{
	free(tree->node_arr);
	return (0);
}

size_t AST_add_node(AST_t *tree, token_t *node, size_t left, size_t right)
{
	size_t new_len = tree->len + 1;
	
	if(new_len >= tree->data_capacity)
	{
	/* Увеличение массива node_arr. 
	 * Алгоритм рассчитан на множество мелких добавлений, поэтому каждый раз
	 * увеличивает объём занятой памяти в 2 раза. Так выделится не более, 
	 * чем 2 раза больше необходимого памяти, но сокращается число дорогих
	 * выделений памяти. */
		size_t new_capacity = 2 * tree->data_capacity;
		tree->node_arr = (AST_node_t*) realloc(tree->node_arr, 
			new_capacity * sizeof(AST_node_t));
		if (tree->node_arr == NULL)
			{ return (1); }
		tree->data_capacity = new_capacity;
	}
	
	tree->node_arr[new_len].node = node;
	tree->node_arr[new_len].left = left;
	tree->node_arr[new_len].right = right;
	
	tree->len = new_len;
	
	return (new_len);
}

/**
 * Синтаксический анализатор реализован нисходящий синтаксический анализ 
 * методом левого рекурсивного спуска.
 * 
 * В синтаксическом анализаторе реализована следующая грамматика:
 * E  => T1 + E | T1 - E | T1
 * T1 => T2 * T1 | T2 / T1 | T2
 * T2 => -T3 | T3
 * T3 => N | (E)
 * Здесь N -- вещественное число.
 * 
 * Основная функция parse_tokens(). Остальные написаны для её реализации.
 * */

/** @brief Создаёт абстрактное синтаксическое дерево из массива токенов
 *  @return Абстрактное синтаксическое дерево
 * */
AST_t parse_tokens (
	/// массив токенов. Должен заканчиваться токеном STOP
	token_t* tok_arr)
{
	AST_t tree;
	AST_init(&tree);
	tree.root = parse_E(&tree, &tok_arr);
	return(tree);
}

/** @brief Выполняет разбор по правилу E  => T1 + E | T1 - E | T1 (сложение и вычитание)
 *  @return индекс, созданной вершины
 * */
size_t parse_E (
	/// указатель на создаваемое дерево
	AST_t* tree, 
	/// указатель на указатель текущего разбираемого токена
	token_t** tok_p)
{
	token_t* op;
	size_t current_root = 0;
	
	size_t t1 = parse_T1(tree, tok_p); /* получаем левую ветвь */
	current_root = t1;
	
	if( (*tok_p)->type == DELIMITER) /* ищем "+" и "-" */
		switch ((*tok_p)->u.name){
			case PLUS:
			case MINUS:
				op = *tok_p;
				next_token(tok_p);
				/* получаем правую ветвь */
				size_t right = parse_E (tree, tok_p);
				current_root = AST_add_node(tree, op, t1, right);
				break;
		}
	return (current_root);
}

/** @brief Выполняет разбор по правилу T1 => T2 * T1 | T2 / T1 | T2 (умножение и деление)
 *  @see parse_E()
 * */
size_t parse_T1 (AST_t* tree, token_t** tok_p)
{
	token_t* op;
	size_t t2 = parse_T2(tree, tok_p); /* получаем левую ветвь */
	size_t current_root = t2;
	
	if( (*tok_p)->type == DELIMITER) /* ищем "*" и "/" */
		switch ((*tok_p)->u.name){
			case MULT:
			case DIV:
				op = *tok_p;
				next_token(tok_p);
				/* получаем правую ветвь */
				size_t right = parse_T1 (tree, tok_p);
				current_root = AST_add_node(tree, op, t2, right);
				break;
		}
	/* Тут обрабатывать ошибку не надо так это не терм верхнего уровня */
	return (current_root);
}

/** @brief Выполняет разбор по правилу T2 => -T3 | T3 (унарный минус)
 * Обратите внимание, что эта функция при необходимости заменяет 
 * токен MINUS на UMINUS, так как лексическому анализатору сложнее определить 
 * унарный минус.
 *  @see parse_E()
 * */
size_t parse_T2 (AST_t* tree, token_t** tok_p)
{
	size_t current_root = 0;
	
	if( (*tok_p)->type == STOP){       /* конец массива токенов */
		current_root = 0;
	}else if( ((*tok_p)->type == DELIMITER) && ((*tok_p)->u.name == MINUS)){
		/* обрабатываем унарный минус */
		(*tok_p)->u.name = UMINUS; /* Меняем имя токена */
		token_t* op = *tok_p;
		next_token(tok_p);
		/* получаем правую ветвь */
		size_t right = parse_T3 (tree, tok_p);
		current_root = AST_add_node(tree, op, 0, right);
	} else {
		current_root = parse_T3 (tree, tok_p);
	}
	/* Обрабатываем ошибку: обнаружение конце массива*/
	if(!current_root){
		printf("ERROR:parse_T2: Empty term?\n");
	}
	
	return (current_root);
}

/** @brief Выполняет разбор по правилу T3 => N | (E) (скобки)
 *  @see parse_E()
 * */
size_t parse_T3 (AST_t* tree, token_t** tok_p)
{
	token_t* op;
	size_t current_root = 0;
	
	if( (*tok_p)->type == STOP){       /* конец массива токенов */
		printf("ERROR:parse_T3: Empty after unary minus?");
	}else if( (*tok_p)->type == LBR){  /* обработка скобок */
		next_token(tok_p);
		current_root = parse_E (tree, tok_p);
		if( (*tok_p)->type != RBR){
			current_root = 0;
			printf("ERROR:parse_T3: lost close brace.\n");
		}else{
			next_token(tok_p);
		}
	}else if( (*tok_p)->type == NUMBER){
		op = *tok_p;
		next_token(tok_p);
		current_root = AST_add_node(tree, op, 0, 0);
	}else{
		printf("ERROR:parse_T3: unknown error\n");
	}
	
	return (current_root);
}
