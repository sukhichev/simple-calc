/*
 * This is Russian description. English description see below.
 * 
 * Лексический анализатор
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * О лицензии читай в файле simple-calc.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Tokinizer
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of simple calc package.
 * 
 * See Copyright in simple-calc.h
 *  
 */

#include <ctype.h>
#include <stdlib.h>
/* Для printf() */
#include <stdio.h>

#include "tokinizer.h"

token_t* next_token (token_t ** tok)
{
	token_t * current = (*tok);
	current++;
	*tok = current;
	return(current);
}


token_t get_token (char **string)
{
	token_t ans;
	ans.type = STOP;
	ans.u.var = NULL;
	
	char *start = *string; /* Начало токена */
	/* Пропускаем пробелы, если есть */
	while (isspace(*start)) start++;
	
	char *end = start;     /* Конец токена */
	char ch = *start;
	if(!ch) {                 /* Проверяем на конец строки*/
		ans.type = STOP;
	} else if(isdigit(ch)){   /* Число */
		ans.type = NUMBER;
		ans.u.val.dval = strtod(start, &end); /* Тут есть специальная функция */
		if (end == start)
		{
			ans.type = ERROR;
		}
	} else if(isalpha(ch)){   /* Переменная */ /* WIP */
		do{
		end++;
		ch = *end;
		} while(ch != '\0' && (isalnum(ch) || ch == '_') );
		
		ans.type = ID;
	} else
	{
		switch (ch){         /* Токены, начинающиеся с конкретного символа */
			case '+':
				ans.type = DELIMITER;
				ans.u.name = PLUS;
				break;
			case '-':
				ans.type = DELIMITER;
				ans.u.name = MINUS;
				break;
			case '*':
				ans.type = DELIMITER;
				ans.u.name = MULT;
				break;
			case '/':
				ans.type = DELIMITER;
				ans.u.name = DIV;
				break;
			case '(':
				ans.type = LBR;
				break;
			case ')':
				ans.type = RBR;
				break;
			default:
				ans.type = ERROR;
				break;
		}
		end++;
	}
	
	*string = end; /* Переставляем указатель */
	
	return (ans);
}


token_t* tokenize (char *string)
{
	size_t capacity = INIT_TOKEN_ARRAY_CAPACITY;
	token_t* ans = (token_t*) malloc(capacity * sizeof(token_t));
	if (ans == NULL)
		{ return (NULL); }
	char **str_p = &string;
	
	size_t len;
	token_t tok;
	tok.type = SPACE;
	for(len = 0; tok.type != STOP; len++){
		tok = get_token(str_p);
		
		if (len >= capacity){ /* расширяем массив при необходимости */
			size_t new_capacity = 2 * capacity;
			ans = (token_t*) realloc(ans, new_capacity * sizeof(token_t));
			if (ans == NULL)
				{ return (NULL); }
			capacity = new_capacity;
		}
		
		ans[len] = tok;
		
		if (tok.type == ERROR){  // Выход на первой же ошибке
			printf("Oops! Error at token %zd\n", (len + 1));
			return (NULL);
		}
	}
	return (ans);
}
