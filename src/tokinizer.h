/*
 * This is Russian description. English description see below.
 * 
 * Заголовочный файл лексического анализатора
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * О лицензии читай в файле simple-calc.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Tokinizer's headers
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of simple calc package.
 * 
 * See Copyright in simple-calc.h
 *  
 */

#ifndef _TOKINIZER_H
#define _TOKINIZER_H	1

typedef struct {
	char *val;
	int len;
} calc_str_t;

typedef union {
	double dval;               /* Для чисел с плавающей запятой */
	calc_str_t *sval;          /* Для строковых */
} calc_value_t;

/* Значения типов и имён токенов */
#undef DEF_TOKEN
#define DEF_TOKEN(a) a,
enum token_type {
#include "token_type.def"
};
enum token_name {
#include "token_name.def"
};


typedef struct{
	int type;                 /* тип токена */
	union {                   /* значение токена */ 
		int name;                /* Для операторов */
		calc_value_t val;        /* Для констант */
		calc_value_t *var;       /* Для переменных */
	} u;
} token_t;

/* Начальные выделяемые объёмы памяти. Не могут быть меньше 1!*/
#define INIT_TOKEN_ARRAY_CAPACITY 64

token_t* next_token (token_t ** tok);
token_t get_token (char **string);
token_t* tokenize (char *string);

#endif /* tokinizer.h */
