/*
 * This is Russian description. English description see below.
 * 
 * Заголовочный файл синтаксического анализатора
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * О лицензии читай в файле simple-calc.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Parser's headers
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of simple calc package.
 * 
 * See Copyright in simple-calc.h
 *  
 */

#ifndef _PARSER_H
#define _PARSER_H	1

#include "tokinizer.h"

typedef struct _AST_node_t{
	token_t *node;
	size_t left;       /* индексы, а не указатели, так как используется realloc*/
	size_t right;
} AST_node_t;


typedef struct{
	size_t len;             /* длина массива данных */
	size_t data_capacity;   /* объём выделенной памяти */
	AST_node_t *node_arr;   /* массив узлов. Заполняется с 1! */
	size_t root;            /* индекс вершины дерева, индекс = 0 -- ошибка */
} AST_t;

/* Начальные выделяемые объёмы памяти. 
 * Не могут быть меньше 2, так как нулевой элемент не используется!*/
#define INIT_AST_CAPACITY 64

/* Интерфейс дерева */
int AST_init(AST_t* tree);
int AST_free(AST_t *tree);
size_t AST_add_node(AST_t *tree, token_t *node, size_t left, size_t right);

/* Публичный интерфейс синтаксического анализатора */
AST_t parse_tokens (token_t* tok_arr);

#endif /* parser.h */
