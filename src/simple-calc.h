/*
 * This is Russian description. English description see below.
 * 
 * Простой калькулятор
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * Это свободная программа. Вы можете распространять, модифицировать,
 * или выполнять эти действия одновременно на условиях
 * 1) cтандартной общественной лицензии GNU (GPL), выпущенной  Фондом 
 * свободного программного обеспечения (FSF) версии 2, или (на ваше усмотрение) 
 * любой более поздней версии;
 * 		ИЛИ
 * 2) русскоязычной GPL-подобной лицензии (пока ещё не написана)
 * 
 * Эта программа распространяется в надежде, что она будет полезной,
 * но без каких-либо явных или подразумеваемых гарантии. Подробнее 
 * смотри cтандартную общественную лицензию GNU (GPL) и 
 * русскоязычную LGPL-подобную лицензию.
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Simple calculator
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * This file is part of simple calc package.
 * 
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of
 * 1) the GNU General Public License (LGPL) as published by the Free Software 
 * Foundation (FSF); either version 2 of the License, or (at your option) 
 * any later version;
 * 		OR
 * 2) Russian GPL-like license (not implemented yet)
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License or Russian GPL-like license for more details.
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307 USA.
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
#ifndef _SIMPLE_CALC_H
#define _SIMPLE_CALC_H	1


#endif /* simple-calc.h */
