/*
 * This is Russian description. English description see below.
 * 
 * Интерфейс командной строки
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета simple calc.
 * 
 * О лицензии читай в файле simple-calc.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Command-line interface
 * 
 * Copyright 2020 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of simple calc package.
 * 
 * See Copyright in simple-calc.h
 *  
 */

#include <stdio.h>
/* Для free() */
#include <stdlib.h>

#include "simple-calc.h"
#include "tokinizer.h"
#include "parser.h"
#include "parserview.h"

/* Публичный интерфейс*/
double calc_string(char * string);
double calc_verbose_string(char * string);

/* Внутренний интерфейс */
double _calc_node(AST_node_t *node_arr, size_t index);
double calc_tree(AST_t tree);


double _calc_node(AST_node_t *node_arr, size_t index)
{
	double first, second;
	AST_node_t elem = node_arr[index];
	switch (elem.node->type){
		case NUMBER:
			return (elem.node->u.val.dval);
		case DELIMITER:
			switch (elem.node->u.name){
				case PLUS:
				case MINUS:
				case MULT:
				case DIV:
					first = _calc_node(node_arr, elem.left);
					second = _calc_node(node_arr, elem.right);
					break;
				case UMINUS:
					second = _calc_node(node_arr, elem.right);
					break;
			}
			switch (elem.node->u.name){
				case PLUS:
					return (first + second);
				case MINUS:
					return (first - second);
				case MULT:
					return (first * second);
				case DIV:
					return (first / second);
				case UMINUS:
					return (-second);
			}
	}
	printf("ERROR:_calc_node: Unknown node");
	return (0);
}

double calc_tree(AST_t tree)
{
	return (_calc_node(tree.node_arr, tree.root));
}

double calc_string(char * string)
{
	token_t* tokens = tokenize (string);
	AST_t tree = parse_tokens (tokens);
	double ans = calc_tree(tree);
	AST_free(&tree);
	free(tokens);
	return (ans);
}

double calc_verbose_string(char * string)
{
	token_t* tokens = tokenize (string);
	printf("\n    Tokens   \n");
	printf("=============\n\n");
	print_token_array(tokens);
	
	AST_t tree = parse_tokens (tokens);
	printf("\n     Tree    \n");
	printf("=============\n\n");
	print_tree(tree);
	
	printf("\n  Draw Tree  \n");
	printf("=============\n\n");
	draw_tree(tree);
	
	double ans = calc_tree(tree);
	printf("\nResult: %lf\n", ans);
	
	AST_free(&tree);
	free(tokens);
	
	return (ans);
}



int main(int argc, char **argv)
{
	char str[80];
	char *ans;
	/* Попробуйте тестовую строку "3*4+(5*6 + 7)" */
	do
	{
		printf(">$");
		ans = fgets(str, 80, stdin);
		calc_verbose_string(str);
	}
	while(ans);
	
	return 0;
}

